<?php

namespace Drupal\site_media_gallery\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;
use Drupal\taxonomy\TermInterface;

/** @package Drupal\site_media_gallery\Controller */
class MediaGalleryController extends ControllerBase {

  /**
   * Страница с каталогом категорий медиагалереи.
   */
  public function catalog() {

    // Формируем список категорий.
    $categories = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'site_media_gallery_catalog']);

    $data = [];
    foreach ($categories as $tid => $term) {

      $media = $term->get('field_cover_image')->entity;

      // Only handle media objects.
      if ($media instanceof MediaInterface) {

        // Загружаем стиль изображения.
        $style = NULL;
        $display_options = EntityViewDisplay::collectRenderDisplay($media, $media->getEntityTypeId())->getComponent('field_media_image');
        if ($display_options['type'] == 'image') {
          $image_style_id = $display_options['settings']['image_style'];
          $style = \Drupal::entityTypeManager()->getStorage('image_style')->load($image_style_id);
        }

        // Получаем идентификатор файла.
        $value = $media->getSource()->getSourceFieldValue($media);
        if (is_numeric($value) && $style instanceof \Drupal\image\Entity\ImageStyle) {
          $file = File::load($value);
          if (!empty($file)) {
            $uri = $file->getFileUri();
            if (!empty($uri)) {
              $data[$tid] = [
                'name' => $term->getName(),
                'url' => $term->toUrl()->toString(),
                'alt' => $media->get('field_media_image')->alt,
                'style_url' => $style->buildUrl($uri),
              ];
            }
          }
        }
      }
    }

    return [
      '#theme' => 'site_media_gallery_catalog',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'site_media_gallery/catalog',
        ],
      ],
    ];
  }

  /**
   * Формирует перечень фото и видеоальбомов по категории.
   */
  public function getMediaGalleriesByTerm(TermInterface $term) {

    // Формируем список альбомов.
    $galleries = \Drupal::entityTypeManager()
      ->getStorage('site_media_gallery')
      ->loadByProperties(['field_category' => $term->id()]);

    $data = [];
    foreach ($galleries as $gallery_id => $gallery) {

      $media = $gallery->getCoverImage();

      // Only handle media objects.
      if ($media instanceof MediaInterface) {

        // Загружаем стиль изображения.
        $style = NULL;
        $display_options = EntityViewDisplay::collectRenderDisplay($media, $media->getEntityTypeId())->getComponent('field_media_image');
        if ($display_options['type'] == 'image') {
          $image_style_id = $display_options['settings']['image_style'];
          $style = \Drupal::entityTypeManager()->getStorage('image_style')->load($image_style_id);
        }

        // Получаем идентификатор файла.
        $value = $media->getSource()->getSourceFieldValue($media);
        if (is_numeric($value) && $style instanceof \Drupal\image\Entity\ImageStyle) {
          $file = File::load($value);
          if (!empty($file)) {
            $uri = $file->getFileUri();
            if (!empty($uri)) {
              $data[$gallery_id] = [
                'name' => $gallery->getTitle(),
                'url' => $gallery->toUrl()->toString(),
                'alt' => $media->get('field_media_image')->alt,
                'style_url' => $style->buildUrl($uri),
              ];
            }
          }
        }
      }
    }

    return [
      '#theme' => 'site_media_gallery_catalog',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'site_media_gallery/module',
          'site_media_gallery/catalog',
        ],
      ],
    ];
  }
}
