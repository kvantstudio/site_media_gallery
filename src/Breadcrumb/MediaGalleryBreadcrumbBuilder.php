<?php

/**
 * @file
 * Contains \Drupal\site_media_gallery\Breadcrumb\MediaGalleryBreadcrumbBuilder.
 */

namespace Drupal\site_media_gallery\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\taxonomy\TermInterface;

/**
 * Class to define the MediaGallery entities pages breadcrumb builder.
 */
class MediaGalleryBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The taxonomy storage.
   *
   * @var \Drupal\Taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs the MediaGalleryBreadcrumbBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * @inheritdoc
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($route_match->getRouteName() == 'entity.site_media_gallery.canonical') {
      return TRUE;
    }
  }

  /**
   * @inheritdoc
   */
  public function build(RouteMatchInterface $route_match) {

    // Ссылка на главную страницу.
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    // Ссылка на каталог фото и видеоальбомов.
    $link = Link::fromTextAndUrl($this->t('Media gallery'), Url::fromUserInput('/media-gallery'));
    $breadcrumb->addLink($link);

    $gallery = $route_match->getParameter('site_media_gallery');
    $tid = $gallery->get('field_category')->target_id;
    $term = $this->termStorage->load($tid);
    if ($term instanceof TermInterface) {
      // @todo This overrides any other possible breadcrumb and is a pure
      // hard-coded presumption. Make this behavior configurable per
      // vocabulary or term.
      $parents = $this->termStorage->loadAllParents($term->id());

      // Remove current term being accessed.
      array_shift($parents);
      foreach (array_reverse($parents) as $parents_term) {
        $parents_term = $this->entityManager->getTranslationFromContext($parents_term);
        $breadcrumb->addCacheableDependency($parents_term);
        $breadcrumb->addLink(Link::createFromRoute($parents_term->getName(), 'entity.taxonomy_term.canonical', array('taxonomy_term' => $parents_term->id())));
      }

      $breadcrumb->addLink(Link::createFromRoute($term->getName(), 'entity.taxonomy_term.canonical', array('taxonomy_term' => $term->id())));
    }

    // This breadcrumb builder is based on a route parameter, and hence it
    // depends on the 'route' cache context.
    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }
}
