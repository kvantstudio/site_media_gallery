<?php

namespace Drupal\site_media_gallery\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\file\Entity\File;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the media image formatter.
 *
 * @FieldFormatter(
 *   id = "site_media_gallery_grid",
 *   label = @Translation("Image grid"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class MediaGalleryGrid extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $media_items = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($media_items)) {
      return $elements;
    }

    $data = [];

    /** @var \Drupal\media\MediaInterface[] $media_items */
    foreach ($media_items as $delta => $media) {
      // Only handle media objects.
      if ($media instanceof MediaInterface) {

        // Загружаем стиль изображения.
        $style = NULL;
        $display_options = EntityViewDisplay::collectRenderDisplay($media, $media->getEntityTypeId())->getComponent('field_media_image');
        if ($display_options['type'] == 'image') {
          $image_style_id = $display_options['settings']['image_style'];
          $style = \Drupal::entityTypeManager()->getStorage('image_style')->load($image_style_id);
        }

        // Получаем идентификатор файла.
        $value = $media->getSource()->getSourceFieldValue($media);
        if (is_numeric($value) && $style instanceof \Drupal\image\Entity\ImageStyle) {
          $file = File::load($value);
          if (!empty($file)) {
            $uri = $file->getFileUri();
            if (!empty($uri)) {
              // Описание изображения.
              $description = trim($media->get('field_summary')->value);
              $description = strip_tags($description);
              $description = str_replace('"', '»', preg_replace('/((^|\s)"(\w))/um', '\2«\3', $description));

              // Атрибут alt изображения.
              $alt = trim($media->get('field_media_image')->alt);
              $alt_exist = TRUE;
              if (empty($alt)) {
                $alt = $media->label();
                $alt_exist = FALSE;
              }

              $data[$delta] = [
                'description' => $description,
                'alt' => $alt,
                'alt_exist' => $alt_exist,
                'url' => \Drupal::service('file_url_generator')->generateAbsoluteString($uri),
                'style_url' => $style->buildUrl($uri),
              ];
            }
          }
        }
      }
    }

    $elements = [
      '#theme' => 'site_media_gallery_grid',
      '#data' => $data,
      '#attached' => [
        'library' => [
          'site_media_gallery/glightbox',
          'site_media_gallery/grig_formatter',
        ],
      ],
    ];

    return $elements;
  }
}
