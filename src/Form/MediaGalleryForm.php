<?php

namespace Drupal\site_media_gallery\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the media gallery entity edit forms.
 */
class MediaGalleryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();

    $message_arguments = ['%label' => $this->entity->label()];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New media gallery %label has been created.', $message_arguments));
    }
    else {
      $this->messenger()->addStatus($this->t('The media gallery %label has been updated.', $message_arguments));
    }

    $form_state->setRedirect('entity.site_media_gallery.canonical', ['site_media_gallery' => $entity->id()]);
  }

}
