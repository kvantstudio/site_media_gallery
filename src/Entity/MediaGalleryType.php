<?php

namespace Drupal\site_media_gallery\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the media gallery configuration entity.
 *
 * @ConfigEntityType(
 *   id = "site_media_gallery_type",
 *   label = @Translation("Media gallery type"),
 *   label_collection = @Translation("Media gallery types"),
 *   handlers = {
 *     "list_builder" = "Drupal\site_media_gallery\MediaGalleryTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\site_media_gallery\Form\MediaGalleryTypeForm",
 *       "add" = "Drupal\site_media_gallery\Form\MediaGalleryTypeForm",
 *       "edit" = "Drupal\site_media_gallery\Form\MediaGalleryTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site configuration",
 *   config_prefix = "site_media_gallery_type",
 *   bundle_of = "site_media_gallery",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/site-media-gallery-types/{site_media_gallery_type}",
 *     "add-form" = "/admin/structure/site-media-gallery-types/add",
 *     "edit-form" = "/admin/structure/site-media-gallery-types/{site_media_gallery_type}/edit",
 *     "delete-form" = "/admin/structure/site-media-gallery-types/{site_media_gallery_type}/delete",
 *     "collection" = "/admin/structure/site-media-gallery-types",
 *   }
 * )
 */
class MediaGalleryType extends ConfigEntityBundleBase {

  /**
   * The entity ID.
   *
   * @var string
   */
  public $id;

  /**
   * The entity label.
   *
   * @var string
   */
  public $label;

}
