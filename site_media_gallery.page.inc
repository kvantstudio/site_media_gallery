<?php

use Drupal\Core\Render\Element;

/**
 * Prepares variables for media gallery templates.
 *
 * Default template: site-media-gallery.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the media gallery
 *     information and any fields attached to the entity.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_site_media_gallery(array &$variables) {
  $variables['site_media_gallery'] = $variables['elements']['#site_media_gallery'];

  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

// /**
//  * Prepares variables for taxonomy-term--site-commerce-catalog.html.twig template.
//  *
//  * @param array $variables
//  */
// function template_preprocess_taxonomy_term__site_commerce_catalog(&$variables) {
//   $variables['view_mode'] = $variables['elements']['#view_mode'];
//   $variables['term'] = $variables['elements']['#taxonomy_term'];
//   /** @var \Drupal\taxonomy\TermInterface $term */
//   $term = $variables['term'];

//   $variables['url'] = !$term->isNew() ? $term->toUrl()->toString() : NULL;

//   // Make name field available separately.  Skip this custom preprocessing if
//   // the field display is configurable and skipping has been enabled.
//   // @todo https://www.drupal.org/project/drupal/issues/3015623
//   //   Eventually delete this code and matching template lines. Using
//   //   $variables['content'] is more flexible and consistent.
//   $skip_custom_preprocessing = $term->getEntityType()->get('enable_base_field_custom_preprocess_skipping');
//   if (!$skip_custom_preprocessing || !$term->getFieldDefinition('name')->isDisplayConfigurable('view')) {
//     // We use name here because that is what appears in the UI.
//     $variables['name'] = $variables['elements']['name'];
//     unset($variables['elements']['name']);
//   }

//   $variables['page'] = $variables['view_mode'] == 'full' && taxonomy_term_is_page($term);

//   // Helpful $content variable for templates.
//   $variables['content'] = [];
//   foreach (Element::children($variables['elements']) as $key) {
//     $variables['content'][$key] = $variables['elements'][$key];
//   }
// }
