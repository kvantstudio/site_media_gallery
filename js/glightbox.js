/**
* @file
* JavaScript for glightbox library.
*/
(function($, Drupal) {

  'use strict';

  const lightbox = GLightbox({
    touchNavigation: true,
    loop: false,
  });


})(jQuery, Drupal);
