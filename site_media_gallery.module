<?php

/**
 * @file
 * Provides a media gallery entity type.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_help().
 */
function site_media_gallery_help($route_name, RouteMatchInterface $route_match) {
  return '';
}

/**
 * Implements hook_theme().
 */
function site_media_gallery_theme($existing, $type, $theme, $path) {
  return [
    'site_media_gallery' => [
      'render element' => 'elements',
      'file' => 'site_media_gallery.page.inc',
      'template' => 'site-media-gallery',
    ],
    'site_media_gallery_grid' => [
      'variables' => ['data' => NULL],
      'file' => 'site_media_gallery.page.inc',
      'template' => 'site-media-gallery-grid',
    ],
    'site_media_gallery_catalog' => [
      'variables' => ['data' => NULL],
      'file' => 'site_media_gallery.page.inc',
      'template' => 'site-media-gallery-catalog',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function site_media_gallery_theme_suggestions_site_media_gallery(array $variables) {
  $suggestions = [];
  $site_media_gallery = $variables['elements']['#site_media_gallery'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'site_media_gallery__' . $sanitized_view_mode;
  $suggestions[] = 'site_media_gallery__' . $site_media_gallery->bundle();
  $suggestions[] = 'site_media_gallery__' . $site_media_gallery->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'site_media_gallery__' . $site_media_gallery->id();
  $suggestions[] = 'site_media_gallery__' . $site_media_gallery->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function site_media_gallery_file_presave(Drupal\file\FileInterface $entity) {
  $uri = $entity->getFileUri();
  if (preg_match("/media-gallery/", $uri) && !preg_match("/foto-/", $uri)) {
    $filename_postprocessor = \Drupal::service('site_media_gallery.filename_postprocessor');
    $new_filename = $filename_postprocessor->process($entity);
    $directory = \Drupal::service('file_system')->dirname($uri);
    $destination = $directory . '/' . $new_filename;
    if ($new_uri = \Drupal::service('file_system')->move($uri, $destination, \Drupal\Core\File\FileSystemInterface::EXISTS_RENAME)) {
      $entity->set('uri', $new_uri);
      $entity->set('filename', \Drupal::service('file_system')->basename($new_uri));
    }
  }
}

/**
 * Implements hook_entity_extra_field_info().
 */
function site_media_gallery_entity_extra_field_info() {
  $extra = [];

  $extra['taxonomy_term']['site_media_gallery_catalog']['display']['site_media_gallery_catalog_galleries'] = [
    'label' => t('Media galleries'),
    'weight' => 10,
    'visible' => TRUE,
  ];

  return $extra;
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function site_media_gallery_taxonomy_term_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($display->getComponent('site_media_gallery_catalog_galleries')) {
    $build['site_media_gallery_catalog_galleries'] = \Drupal::service('site_media_gallery.controller')->getMediaGalleriesByTerm($entity);
  }
}
